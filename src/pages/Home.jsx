import React from "react";

import {LiLink} from "../components/LiLink"

const Home = () => {
    return (
        <ul>
            <LiLink />
        </ul>
    );
};

export {Home};